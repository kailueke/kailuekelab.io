---
author: pothos
comments: true
date: 2010-10-21 17:16:50+00:00
layout: page
slug: about
title: Contact
---

**E-Mail:**


kailueke류크 카이 'at' riseup.net (only using Latin font)


**Rechtliches**





Gemäß §28 BDSG widerspreche ich jeder kommerziellen Verwendung und Weitergabe meiner Daten.




Die_Der Autor_in übernimmt keinerlei Gewähr für die Aktualität, Korrektheit, Vollständigkeit oder Qualität der bereitgestellten Informationen. Haftungsansprüche gegen die_den Autor_in, welche sich auf Schäden materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollständiger Informationen verursacht wurden sind grundsätzlich ausgeschlossen, sofern seitens der_des Autor_ins kein nachweislich vorsätzliches oder grob fahrlässiges Verschulden vorliegt. Alle Angebote sind freibleibend und unverbindlich. Die_Der Autor_in behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.




Mit Urteil vom 12. Mai 1998 hat das Landgericht Hamburg entschieden, dass man durch die Veröffentlichung eines Links die Inhalte der gelinkten Seite ggf. mit zu verantworten hat. Dies kann, so das Landgericht, nur dadurch verhindert werden, dass man sich ausdrücklich von diesen Inhalten distanziert. Dies wird hiermit getan. Die_Der Autor_in hat verschiedene Links zu anderen Internetseiten gelegt. Für alle externen Links gilt: Die_Der Autor_in hat keinerlei Einfluss auf Gestaltung und Inhalte der verlinkten Seiten.



