---
author: pothos
comments: false
date: 2017-01-02 20:39:18+00:00
layout: page
slug: other-works
title: Works
---

[*Academic Writings in Computer Science*](https://pothos.github.io/papers/)

[*Software Projects on GitHub*](https://github.com/pothos)

Most writings are published elsewhere (e.g. articles or statements of associations), e.g., see [Korea Forum 2015](http://www.koreaverband.de/shop/korea-forum-2015/) [(PDF)](http://www.koreaverband.de/wp-content/uploads/2016/04/KF_2015_Editorial.pdf).

Selected affiliations: [FIfFKon14](https://2014.fiffkon.de/) and [FIfFKon16](https://2016.fiffkon.de/) (organisation).
