---
author: pothos
comments: true
date: "2017-07-25T19:46:30Z"
slug: gnome-disks-integrate-resize-and-repair
tags:
- Disks
- gnome
- GSoC
title: 'GNOME Disks: Integrate Resize and Repair'
---

The basic prototype patches let GNOME Disks resize partitions and their filesystems, check filesystems and repair them – at least when built against UDisks master where the actual work is performed. It needs some fixes on how jobs and UI states correspond but here a glimpse on how the resize looks like.

[![](/images/2017-07-25-gnome-disks-integrate-resize-and-repair/resize-300x236.png)](/images/2017-07-25-gnome-disks-integrate-resize-and-repair/resize.png)

The current pending mockups for the whole UI will be discussed in person at GUADEC. Where to show the result of the filesystem check or repair and where to show the jobs are some of the questions.

In the implementation some problems pop up as usual, this time with some impact on the robustness but hopefully nothing again at the level of dealing with libparted geometry configuration.
The prototype needs some polishing as well, and in the underlaying layers I would like to add NTFS support and improve the FAT resize support. Finally it would be nice if the UDisks jobs indicate the progress which is not supported in libblockdev yet.

Resizing other block devices than partitions is also left to do (e.g. LUKS, backing loop file). Offering a filesystem resize where the block device can't be fitted is a strange experience because it's in general impossible to tell up to which size a filesystem occupies space in this block, meaning the result would look the same except for a difference in free space. Thus for the beginning only filesystems in partitions are resizable.

**Development News**

Recently 3.25.4 was released and the last changes improved the build experience a lot – Iñigo Martínez gave a meson port as surprise gift. Let's see whether it's enough time for the ongoing work to land on master for 3.26.

<hr>

*Migrated comments:*

* Stu says:<br>
  26th July 2017 at 02:03<br>
  This is a great start, definitely looks much more useful for managing removable devices like USB sticks.
* Debarshi Ray says:<br>
  28th July 2017 at 23:17<br>
  Exciting! Thanks for working on GNOME Disks.
* biosin says:<br>
  29th July 2017 at 00:11<br>
  Appreciate the work. Will it also be possible to move partitions and queue multiple operations like in gparted? Like resize and move in one step.
* Kai says:<br>
  29th July 2017 at 01:31<br>
  Moving is currently not planned, but it would be possible in a similar way the save/restore is working. Queueing up would not be needed if the move can also be directly specified during the resize. We should consider this for the future!
