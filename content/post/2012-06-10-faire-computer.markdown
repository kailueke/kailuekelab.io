---
author: pothos
comments: true
date: "2012-06-10T15:22:28Z"
slug: faire-computer
tags:
- behind the screen
- blood in the mobile
- Elektroschrott
- Konfliktmineralien
title: „Faire“ Computer?
---

Foxconn bekommt durch Apple in letzter Zeit immer mehr Aufmerksamkeit bezüglich der Gewerkschaftsrechte und Arbeitsbedingungen der Fabriken in China. Gerade weil iLifestyle und Suizid so weit voneinander weg zu liegen scheinen. Kampagnen wie [makeITfair](http://makeitfair.org/) (Germanwatch e.V.) u.a. sowie der Greenpeace »Guide to Greener Electronics« werfen den Blick auf die Schattenseiten der Technik in Herkunft der Materialien, Produktionsbedingungen, Giftstoffen, Energieverbrauch, Entsorgung und die mitgekaufte Verantwortung. Obwohl auch in Deutschland Reuse vor Recycle gilt, wird meist schon das Recycling in den Anfängen der abwechselnd von den Herstellern organisierten Entsorgungskette erstickt.

Gestern sah ich den Film [»Blood in the Mobile«](http://bloodinthemobile.org/), der die für Elektronikprodukte essentiellen »Konfliktmineralien« aus dem Kongo zum Thema hat. Aufgebracht durch einen UN-Expertenbericht, der aussagt, dass ein direkter Zusammenhang zwischen Minenabbau für Mobiltelefone und Angriffen auf die Bevölkerung durch Milizen besteht (UN Group of Experts, Dez. 2008, S/2008/43), tritt Regisseur Frank Piasecki Poulsen mit dem Mobiltelefonhersteller seiner Wahl in Kontakt. Aber woher die verwendeten Mineralien stammen, kann und will niemand sagen, obwohl seit 10 Jahren das Problem intern diskutiert wird - zudem sei auch die ganze Elektroniksparte betroffen und auch bei den Konsumierenden läge die Verantwortung. Poulsen will verstehen und mit eigenen Augen sehen, wie die Realität vor Ort aussieht und bereist mit Kamerateam eine der Minen und zeigt somit die verschiedenen Problematiken auf. Lösungsansätze werden auch diskutiert und als Anfang ist durch einen US-Gesetzesantrag erreicht worden, dass die Zulieferungskette transparent veröffentlicht werden muss. Eine Projektseite ist [hier](http://www.raisehopeforcongo.org/).

Relativ neu ist die Doku [»Behind the Screen: Das Leben meines Computers«](http://behindthescreen.at/), welche einige Aspekte der Herstellung und Entsorgung beispielhaft abdeckt. Da EU-Exporte für Elektroschrott verboten sind, landen als Gebrauchtware deklarierte Müllladungen auf Halden in Ghana und werden in Accra, Koforidua usw. zerlegt. Dieser Prozess setzt höchstgiftige Stoffe frei und betrifft somit die dort arbeitenden Kinder mehrfach. Auch die Bleikonzentration und weitere Kunststoffe wie Weichmacher belasten das Trinkwasser.

Doch bevor ein Device dort landet, wird es z.B. in Tschechien von vietnamesischen Leiharbeitern in Knebelverträgen hergestellt. Das verwendete Gold könnte aber wieder aus Ghana kommen. Hauptsache, wir sind auf der „richtigen“ Seite des digital divide? Mehr zum Thema Informatik & Gesellschaft auch [hier](http://fiff.de/publikationen/fiff-kommunikation), in der Zeitschrift des »Forums InformatikerInnen für Frieden und gesellschaftliche Verantwortung e.V.«.
