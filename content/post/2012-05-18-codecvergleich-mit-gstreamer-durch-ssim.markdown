---
author: pothos
comments: true
date: "2012-05-18T01:09:31Z"
slug: codecvergleich-mit-gstreamer-durch-ssim
tags:
- Ausarbeitung
- Dirac
- GStreamer
- Qualität
- SSIM
- Video
title: Codecvergleich mit GStreamer durch SSIM
---

Während der Wirrungen bezüglich des HTML5-Videostandards hatte ich (noch lange bevor WebM existierte) einen Blick auf [Dirac](https://de.wikipedia.org/wiki/Dirac_%28Codec%29) geworfen, einen alternativen Codec der BBC Research Labs. Nach subjektiven Vergleichen und einiger Benutzung, fehlte mir dennoch ein Verfahren, um mehr verlässliche Aussagen über die Qualität (sowie zu erwartende Größe) eines komprimierten Videos zu machen. Anstelle des „alten“ [PSNR](https://en.wikipedia.org/wiki/Peak_signal-to-noise_ratio) wurde [SSIM](https://en.wikipedia.org/wiki/Structural_similarity) zur Differenzanalyse zwischen Original und dem komprimierten Bild verwendet. [GStreamer](https://en.wikipedia.org/wiki/GStreamer) bietet sich durch starke Flexibilität an - ein Testframework wurde in [Python](https://de.wikipedia.org/wiki/Python_%28Programmiersprache%29) mit [Django](https://www.djangoproject.com/) umgesetzt. Leider steht der Out-of-the-box-Verwendung noch einiges im Wege; gerade die SSIM-Berechnung ist noch nicht als eigenständiges GStreamer-Element implementiert, was Geschwindigkeit und Stabilität verbessern würden. Auch die generelle Anbindung der GStreamer-Mainloop in die Webapplikation wäre besser durch einen daemon zu lösen. Daher kommt hier jetzt noch kein Quellcode…

[Ausarbeitung (Kurs Digitales Video): Der Dirac-Wavelet-Codec und automatisierter Qualitätsvergleich mit Hilfe des GStreamer-Frameworks](/ausarbeitung-kailueke-dirac-vergleich-gstreamer.pdf)

Die erhobenen Daten sind nur beispielhaft und erheben keinen Anspruch auf vollständige Charakterisierung der Codecs. Denn der Zeitaufwand für SSIM ist groß - und somit ist die Eingabesequenz sehr kurz gewählt.
