---
author: pothos
comments: false
date: "2018-01-21T13:52:12Z"
slug: interaction-between-the-user-and-kernel-space-in-linux
tags:
- Linux
- paper
title: Interaction Between the User and Kernel Space in Linux
---

_Seminar Paper June 2017, Interaction Between the User and Kernel Space in Linux, Kai Lüke_

**Abstract:**


<blockquote>System calls based on context switches from user to kernel space are the established concept for interaction in operating systems. On top of them the Linux kernel offers various paradigms for communication and management of resources and tasks. The principles and basic workings of system calls, interrupts, virtual system calls, special purpose virtual filesystems, process signals, shared memory, pipes, Unix or IP sockets and other IPC methods like the POSIX or System V message queue and Netlink are are explained and related to each other in their differences. Because Linux is not a puristic project but home for many different concepts, only a mere overview is presented here with focus on system calls.</blockquote>


Download paper: [Interaction Between the User and Kernel Space in Linux, Kai Lüke](https://pothos.github.io/papers/linux_userspace_kernel_interaction.pdf)

Converted to HTML with pdf2htmlEX: [Interaction Between the User and Kernel Space in Linux, Kai Lüke](https://pothos.github.io/papers/linux_userspace_kernel_interaction.pdf2htmlEX.html)
