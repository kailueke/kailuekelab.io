---
author: pothos
comments: true
date: "2010-10-23T18:49:19Z"
slug: facharbeit
title: Facharbeit
---

Ich habe meine Facharbeit in Mathe geschrieben zum Thema _Public-Key-Kryptographie am Beispiel des RSA-Verfahrens: heutige Umsetzung und Implementierung_.

Hier sind die Links:

[Facharbeit als PDF](/facharbeitrsakailueke.pdf)

[schnelles hybrides Verschlüsselungs-Programm aufbauend auf dem Facharbeits-Code (src)](/rsa.cpp)

[das kompilierte Programm für Linux (64 Bit)](/rsa.zip) braucht die Ubuntu-Pakete libgmp3c2 libgmpxx4ldbl und zum Kompilieren libgmp3-dev

[und das kompilierte Programm für Linux (32 Bit)](/rsa32.zip)

[das kompilierte Programm für Windows](rsa.exe.zip)
