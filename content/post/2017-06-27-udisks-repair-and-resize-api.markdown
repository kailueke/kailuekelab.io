---
author: pothos
comments: true
date: "2017-06-27T08:54:05Z"
slug: udisks-repair-and-resize-api
tags:
- Disks
- gnome
- GSoC
title: UDisks Repair and Resize API
---

Almost all actions in GNOME Disks rely on the UDisks system service. That way the authorization policy is handled by PolKit and Disks does not have to spawn any root processes. Because format and mount jobs are registered there to the device objects it is possible to initiate them from command line via _gio mount_, _udisksctl_ or another D-Bus client such that Disks can still show these ongoing operations in the UI.

There are cases where D-Bus services are not possible (e.g. in an OS installer) and therefore libblockdev is a new library with similar functionality. The last UDisks release offloads many tasks to it instead of spawning utility binaries itself. In libblockdev the preference is to reuse libraries like libparted if possible and otherwise spawn utility binaries.

To have resize and repair support in Disks I had to introduce querying for installed support of an action and the action itself in UDisks and thus in libblockdev. The advantages are only small changes in Disks and Cockpit will also benefit. Now there are PRs pending for a generic repair and check function, a partition resize function and query functions for repair, resize and consistency check support of filesystem types in libblockdev. Then the PRs pending for UDisks can make use of them to expose the following API methos:

    
    org.freedesktop.UDisks2.Manager:
    CanFormat(fstype) -> (available, required_utility)
    CanResize(fstype) -> (available, mode, required_utility)
    CanCheck(fstype) -> (available, required_utility)
    CanRepair(fstype) -> (available, required_utility)
    
    org.freedesktop.UDisks2.Partition:
    Resize(size, options)
    
    org.freedesktop.UDisks2.Filesystem:
    Resize(size, options)
    Check(options) -> consistent
    Repair(options) -> repaired


The resize mode flags indicate if online or offline shrinking/growing is supported. The size argument for partitions should guarantee that content of this size will fit in and alignment might only make it bigger. The progress percentage property of the running job is not yet set because this needs parsing stdout in libblockdev, so just a spinner in Disks for now. It's possible to test them from d-feet, e.g. to see if the F2FS format is available and what utility is missing if not:

[![](/images/2017-06-27-udisks-repair-and-resize-api/format_missing_f2fs-300x153.png)](/images/2017-06-27-udisks-repair-and-resize-api/format_missing_f2fs.png)

On the command line it would be _gdbus_, _mdbus2_ or _dbus-send_ or the D-Bus library of your choice (e.g. in Python3 _from gi.repository import Gio_). Here assuming an almost empty unmounted ext4 partition 1 on a loop device:

    
    $ # repair first (the option dict {} is unused)
    $ gdbus call --system --dest org.freedesktop.UDisks2 \
     --object-path /org/freedesktop/UDisks2/block_devices/loop0p1 \
     --method org.freedesktop.UDisks2.Filesystem.Repair {}
    (true,)
    
    $ # shrink filesystem to 50 MB
    $ gdbus call --system --dest org.freedesktop.UDisks2 \
     --object-path /org/freedesktop/UDisks2/block_devices/loop0p1 \
     --method org.freedesktop.UDisks2.Filesystem.Resize 50000000 {}
    ()
    
    $ # shrink partition to 50 MB
    $ gdbus call --system --dest org.freedesktop.UDisks2 \
     --object-path /org/freedesktop/UDisks2/block_devices/loop0p1 \
     --method org.freedesktop.UDisks2.Partition.Resize 50000000 {}
    ()


**GNOME Disks Development News**

After Disks 3.25.2 was released there were only small changes in master. The standby menu entry was fixed to be usable again after the action took effect. Mitchell took care of large file support on 32-bit systems, an interesting topic. The biggest current change is the new format dialog. The new UDisks API must be integrated now in the UI. It would be nice to get NTFS (and progress) support in libblockdev and finally the Disks UI mockups done as well.

<hr>

*Migrated comments:*

* Emmanuele Bassi says:<br>
  27th June 2017 at 13:50 Uhr<br>
  I strongly urge people working on udisk to actually fix their build system, and to *test* all permutations.<br>
  I had to pin udisks in GNOME Continuous because of libblockdev and udisk constantly breaking and being tragically bad with non-srcdir builds (which tells me nobody runs `make distcheck` either); jhbuild users are pinned to their system version of udisk for the same reason.<br>
  This means that gnome-disk-utility changes won’t be buildable by Continuous and jhbuild.<br>
  „It builds on my laptop and when I make an RPM package for RHEL/Fedora“ is not a sustainable development model.
* Sébastien Wilmet says:<br>
  27th June 2017 at 20:39<br>
  It’s great that someone works again on gnome-disk-utility! I hope that after your studies you’ll be hired by a company working on GNOME 😉
