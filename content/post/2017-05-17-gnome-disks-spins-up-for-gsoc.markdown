---
author: pothos
comments: true
date: "2017-05-17T12:09:55Z"
slug: gnome-disks-spins-up-for-gsoc
tags:
- Disks
- gnome
- GSoC
title: GNOME Disks spins up for GSoC
---

This year's [Google Summer of Code](https://summerofcode.withgoogle.com/) features many valuable open source organizations and GNOME is one of them.
In [GNOME's project list](https://wiki.gnome.org/Outreach/SummerOfCode/2017/Projects) you can find Disks, the GNOME utility for dealing with storage devices.
I'm happy that it worked out, because at first it was not in the list of ideas. The fact that Disks was considered unmaintained bothered me, but also was a big hint to finally get involved instead of mere bug reporting. So the plan emerged to propose working on two key features: [Resize and repair filesystems](https://wiki.gnome.org/Outreach/SummerOfCode/2017/Projects/KaiLueke_Disks).

Getting in contact with people through open bugs, IRC and the mailing lists went well and I'm grateful for the support (extra thanks to Michael!). Ondrej stepped in as mentor and made this possible, I already learned a lot and just took over maintainership. In the rest of this post I write a bit about the GSoC project outline and recent development in GNOME Disks.

[![](/images/2017-05-17-gnome-disks-spins-up-for-gsoc/disks.png)](/images/2017-05-17-gnome-disks-spins-up-for-gsoc/disks.png)

**Goal:**
GNOME Disks should support repairing and resizing filesystems through UDisks, thus
enabling non-root users to safely set up their devices from a graphical interface.

**Motivation:**
GParted does not use UDisks, runs only as root and thus not under Wayland.

**Outline**

  * Work out new D-Bus interface functions in cooperation with the UDisks team so that
UDisks offers resize/repair through libblockdev (FS plugin) and is also tooling aware by
probing relevant binaries. The exact solution is to be negotiated, since Cockpit might
make use of it too.
  * Use that new UDisks functionality in GNOME Disks, design and implement UI (follow
HIG, good progress indication)
  * Focus on supporting just a few filesystems in the beginning


The first task is to rework the format dialog, which should stick with the principle to offer just a few sane defaults but also not forbid to choose others (list available filesystems and find a good solution for missing tooling).

[![](/images/2017-05-17-gnome-disks-spins-up-for-gsoc/format-redesign-300x209.png)](/images/2017-05-17-gnome-disks-spins-up-for-gsoc/format-redesign.png)


[![](/images/2017-05-17-gnome-disks-spins-up-for-gsoc/format-redesign-select-300x209.png)](/images/2017-05-17-gnome-disks-spins-up-for-gsoc/format-redesign-select.png)
[![](/images/2017-05-17-gnome-disks-spins-up-for-gsoc/format-redesign-select-more-300x209.png)](/images/2017-05-17-gnome-disks-spins-up-for-gsoc/format-redesign-select-more.png)
[![](/images/2017-05-17-gnome-disks-spins-up-for-gsoc/format-redesign-select-other-300x209.png)](/images/2017-05-17-gnome-disks-spins-up-for-gsoc/format-redesign-select-other.png)(Needs Radio Buttons for the selection.)


**Development News**

Translation work is constantly going on – great to see that!

I've set up a [GNOME Wiki page as website for Disks](https://wiki.gnome.org/Apps/Disks).

[![](/images/2017-05-17-gnome-disks-spins-up-for-gsoc/wikipage-114x300.png)](/images/2017-05-17-gnome-disks-spins-up-for-gsoc/wikipage.png)

A bug preventing unmounting and locking is fixed as well as a bug in the format dialog.

Taking care of loop devices:

  * Explain the auto-clear option, i.e. detaching loop devices if they are not mounted anymore
  * Disable auto-clear during operations if it would lead to unwanted detachment
  * Create a new empty disk image from the AppMenu


UI improvements:

  * Show UUID
  * Hints for passphrase missmatch
  * Indicate maximal FS label length
  * Explain the meaning of the mounting and encryption options, i.e. overwriting the session defaults with fstab/crypttab entries
  * Use mimetypes for disk image selection (welcome Mitchell as new contributor!)


_There are many ways to contribute to GNOME, please support the [Outreachy](https://wiki.gnome.org/Outreachy) by spreading the word!_
