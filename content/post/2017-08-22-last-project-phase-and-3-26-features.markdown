---
author: pothos
comments: true
date: "2017-08-22T10:32:32Z"
slug: last-project-phase-and-3-26-features
tags:
- Disks
- gnome
- GSoC
title: Last Project Phase and 3.26 Features
---

Repair and resize is available in the recent 3.25 release and needs at least UDisks 2.7.2. Currently Ext4, XFS and FAT are supported through libblockdev and I hope to extend this list with NTFS soon. There were some race conditions when a resized partition is detected by the kernel again and also the FAT support through libparted is still a bit shaky.
Showing the proportion of used disk space in the slider was the last appearance change for the resize dialog.
I've [written some retrospective words about the project at the end of its wiki page](https://wiki.gnome.org/Outreach/SummerOfCode/2017/Projects/KaiLueke_Disks) – thank to all for this learning experience!

[![](/images/2017-08-22-last-project-phase-and-3-26-features/new-resize-300x250.png)](/images/2017-08-22-last-project-phase-and-3-26-features/new-resize.png)

The new format dialog did not get merged yet and will come to master after the freeze.
Not yet implemented are the mockups for the whole UI where the partition list is shown. Jimmy Scionti and others in #gnome-design worked on my changes to Allan's original mockup and the [direction](https://bugzilla.gnome.org/show_bug.cgi?id=737671) seems to stabilize now.

[![](/images/2017-08-22-last-project-phase-and-3-26-features/new-disks-mockup-300x225.png)](/images/2017-08-22-last-project-phase-and-3-26-features/new-disks-mockup.png)[![](/images/2017-08-22-last-project-phase-and-3-26-features/disks-partitions-300x278.png)](/images/2017-08-22-last-project-phase-and-3-26-features/disks-partitions.png)
[![](/images/2017-08-22-last-project-phase-and-3-26-features/not-a-pie-chart-300x235.png)](/images/2017-08-22-last-project-phase-and-3-26-features/not-a-pie-chart.png)

It was nice to visit GUADEC and meet people in person and discuss various things in the days after as well. There are too many areas where I also would like to do something but rightnow the time is short, and the list of plans for Disks is also growing…

**Development News**

Both 3.25.90 and 3.25.91 have been [released](https://git.gnome.org/browse/gnome-disk-utility/tree/NEWS) and I think that 3.26 will be a good improvement compared to 3.24. Please report issues you experience.

<hr>

*Migrated comments:*

* Stu says:<br>
  22nd August 2017 at 17:31<br>
  Fantastic 🙂<br>
  One thing I think no disk utilities seem to do is differentiate between say USB sticks and SD cards and the internal disk.<br>
  Since it takes exactly the same steps to format both, I’m always worried I might not be paying attention and accidentally click through and kill the computers disk, when I meant to repartition yet another SD card.<br>
  I guess this is a question for design though.
* Kai says:<br>
  23rd August 2017 at 13:13<br>
  When starting Disks there are different icons on the left side for the devices and also their names should help to distinguish.The format dialog asks for confirmation where you will see the device described again.
