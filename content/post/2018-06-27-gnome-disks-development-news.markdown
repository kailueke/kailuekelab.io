---
author: pothos
comments: true
date: "2018-06-27T01:04:31Z"
slug: gnome-disks-development-news
tags:
- Disks
- gnome
title: GNOME Disks Development News
---

The most visible change for [GNOME Disks](https://wiki.gnome.org/Apps/Disks) in 3.28 was the new format dialog. Unfortunately the redesign of the partition view has not been tackled in the last development cycle.

A contribution from Andrea Azzarone improves the experience with snap applications because GNOME Disks will now hide all images mounted with the x-gdu.hide option, e.g.
`sudo mount special.img /mnt/test/ -o loop,x-gdu.hide`

Also several bugs are fixed in 3.28.3, so I recommend distributions to update. A Flatpak release is not yet available and due to some minor issues regarding the integration with the host system I don't know yet if this really makes much sense.

For 3.30 there is support for opening VeraCrypt images, which was contributed by segfault and the Tails project. This needs a UDisks version which is not yet available and requires the creation of the file `/etc/udisks2/tcrypt.conf` if not shipped by the distribution.

Finally, the [gnome-disk-utility repository](https://gitlab.gnome.org/GNOME/gnome-disk-utility) has been moved to GNOME's GitLab. The repository also contains the code for gnome-disk-image-mounter and the gsd-disk-utility-notify service for S.M.A.R.T. events. I hope this will attract some more people to work on GNOME Disks as I am currently not able to do much for it.
