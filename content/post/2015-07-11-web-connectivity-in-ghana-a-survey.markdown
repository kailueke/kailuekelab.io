---
author: pothos
comments: true
date: "2015-07-11T17:14:49Z"
slug: web-connectivity-in-ghana-a-survey
tags:
- Ghana
- internet
- latency
- paper
title: Web Connectivity in Ghana - A survey
---

**Abstract:**


<blockquote>Starting from the research “Dissecting web latency in Ghana” of Zaki et al. the field of Internet connectivity in Ghana is explored and the different aspects like latency, DNS resolution times, complexity of web sites, caching and prefetching, TLS/SSL, TCP and cellular network performance, role of ISPs, Internet Exchange Points, are brought in relation. Measurement approaches and tailored solutions for areas with bad connectivity are listed. Possible ways of improvement are discussed.</blockquote>


Download paper: [Web Connectivity in Ghana - A survey, Kai Lüke](https://pothos.github.io/papers/web-connectivity-in-ghana-a-survey.pdf)

Converted to HTML with pdf2htmlEX: [Web Connectivity in Ghana - A survey, Kai Lüke](https://pothos.github.io/papers/web-connectivity-in-ghana-a-survey.pdf2htmlEX.html)

**Categories and Subject Descriptors:** C.2.6 [Internetworking]: Standards; C.4 [Performance of Systems]: Reliability, availability, and serviceability

**Keywords:** Developing Countries, Internet, Latency, Performance, TCP, Mobile, Cellular
