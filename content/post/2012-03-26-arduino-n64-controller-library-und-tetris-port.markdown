---
author: pothos
comments: true
date: "2012-03-26T02:24:05Z"
slug: arduino-n64-controller-library-und-tetris-port
tags:
- arduino
- n64 controller
- tvout
title: Arduino N64 Controller Library und Tetris-Port
---

**Moved to GitHub:** [https://github.com/pothos/arduino-n64-controller-library](https://github.com/pothos/arduino-n64-controller-library),
*except for [N64Tetris](/N64Tetris.zip)*

Die [Arduino-Plattform](http://de.wikipedia.org/wiki/Arduino-Plattform) ([arduino.cc](http://arduino.cc/)) bietet einen einfachen Einstieg in die Microcontroller-Programmierung und ist gerade [im Nicht-Geek-Bereich eine Erfolgsgeschichte](http://blog.makezine.com/2011/02/10/why-the-arduino-won-and-why-its-here-to-stay/). Die vielen Bibliotheken ermöglichen es, hauptsächlich auf der "Organisationsebene" zu bleiben - in kurzer Zeit war aus der Kombination von [TVout](http://code.google.com/p/arduino-tvout/), dem verfügbaren Tetris-Clone und einigen Modifikationen zum Speichern der Highscore im [EEPROM](http://arduino.cc/playground/Code/EEPROMWriteAnything) eine archaische Spielkonsole aufgebaut. [Dank dieser Anleitung](http://www.instructables.com/id/Use-an-Arduino-with-an-N64-controller/) konnte ein N64-Controller benutzt werden. Während für NES relativ viel Quellcode [verfügbar ist](http://code.google.com/p/nespad/), gibt es keine komfortable N64-Controller-Library, da wegen exaktem Timing Inline-Assembler verwendet wird und der PIN fest eingeschrieben ist. Meine Bibliothek ist vielleicht nicht in schönster Manier geschrieben und alles andere als optimal in der Codegröße, aber für eine Erstveröffentlichung sollte es ausreichen - komfortabel ist sie zumindest (auch alle möglichen PINs von 0 bis 13 können unabhängig voneinander für Controller benutzt werden).

Die Lizenz ist unklar, es handelt sich hauptsächlich um Modifikationen und Zusammenstellungen. Der Tetris-Port "Simple Tetris Clone" ist z.B. unter der MIT-License veröffentlicht, die meisten Bibliotheken unter der (L)GPL.

Version <del>1 der Library (26.03.2012)</del> 2 der Library (26.07.2012),

getestet auf Arduino Uno: [N64Controller](https://github.com/pothos/arduino-n64-controller-library)

Hier der Quelltext für das Spiel: [N64Tetris](/N64Tetris.zip)

Beispielquellcode:

    
    #include <N64Controller.h>
    
    N64Controller player1 (12); // PIN 12
    
    void setup() {
        player1.begin(); // Initialisierung
    }

    void loop() {
        delay(30);
        player1.update(); // Tastenzustand auslesen
        if (player1.button_A() && player1.button_D_down()
            || player1.button_Start()) { // gelesene Tasten abfragen
    
            int xachse = player1.axis_x(); // kann negativ oder positiv sein,
                                           // je nach Ausrichtung des Analog-Sticks
        }
    
        // …
    }
