---
author: pothos
comments: false
date: "2016-10-18T22:49:42Z"
slug: design-of-a-python-subset-compiler-in-rust-targeting-zpaql
tags:
- compiler
- compression
- paper
- rust
- zpaq
title: Design of a Python-subset Compiler in Rust targeting ZPAQL
---

_B.Sc. Thesis, Computer Science Freie Universität Berlin, Kai Lüke_

**Abstract:**


<blockquote>The compressed data container format ZPAQ embeds decompression algorithms as ZPAQL bytecode in the archive. This work contributes a Python-subset compiler written in Rust for the assembly language ZPAQL, discusses design decisions and improvements. On the way it explains ZPAQ and some theoretical and practical properties of context mixing compression by the example of compressing digits of π. As use cases for the compiler it shows a lossless compression algorithm for PNM image data, a LZ77 variant ported to Python from ZPAQL to measure compiler overhead and as most complex case an implementation of the Brotli algorithm. It aims to make the development of algorithms for ZPAQ more accessible and leverage the discussion whether the current specification limits the suitability of ZPAQ as an universal standard for compressed archives.</blockquote>


Download paper: [Design of a Python-subset Compiler in Rust targeting ZPAQL, Kai Lüke](https://pothos.github.io/papers/BSc_thesis_ZPAQL_compiler.pdf)

Converted to HTML with pdf2htmlEX: [Design of a Python-subset Compiler in Rust targeting ZPAQL, Kai Lüke](https://pothos.github.io/papers/bsc_thesis_zpaql_compiler.pdf2htmlEX.html)
