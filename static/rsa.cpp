/*
 *      rsa.cpp
 *      RSA-Beispielimplementierung durch Benutzung der GNU Multiple Precision Arithmetic Library <gmplib.org>
 *      Copyright (C) 2009 Kai Lüke <kai-tobias@web.de> oder auf kailueke.de.vu
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *      
 *      Kompilieren: g++ -Wall -O3 -s -march=native -o rsa rsa.cpp" -lgmpxx -lgmp
 *      Die Angaben -Wall -O3 -s -march=native sind optional. Wichtig ist jedoch, dass
 *      die GMP Library verfügbar ist (mehr dazu auf http://gmplib.org/manual/Installing-GMP.html#Installing-GMP)
 *      bzw. das Ubuntu Paket libgmpxx4ldbl o.ä. installiert ist.
 *      Zeichensatz: UTF-8
 *      
 *      Das Programm ist enthält nur die RSA-Grundlagen und ist in dieser Form noch nicht
 *      zur Praktischen Anwendung geeignet. Es fehlen:
 *      - OAEP
 *      - ein Protokoll
 *      - Hybride Verschlüsselung mit AES, Twofish, Serpent o.ä.
 *      Das alles liefert der OpenPGP-Standard, welcher von GnuPG unterstützt wird,
 *      daher empfehle ich die Benutzung von GnuPG.
 */

const float VERSION=0.2f;

#include <iostream>
#include <limits>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <gmp.h>
#include <gmpxx.h>
#include <cstdlib>
#include <ctime>

using namespace std;

// Der Erweiterte Euklidische Algorithmus
// gibt die Zahlen x, y und den ggT(a, b) für die
// Gleichnung ggT(a, b) = x·a + y·b zurück
mpz_class* erwEuklid(mpz_class a, mpz_class b){
	mpz_class x=0;
	mpz_class lastx=1;
	mpz_class y=1;
	mpz_class lasty=0;
	mpz_class temp;
	mpz_class quotient;
	while(b!=0){
		temp=b;
		quotient=a/b;
		b=a%b;
		a=temp;
		temp=x;
		x=lastx-quotient*x; // Entspricht der Umformung
		lastx=temp;
		temp=y;
		y=lasty-quotient*y; // und Einsetzung nach ggT=x·a+y·b
		lasty=temp;
	}
	mpz_class *ret=new mpz_class[3];
	ret[0]=lastx;
	ret[1]=lasty;
	ret[2]=a;
	return ret;
}





// Diese Klasse ist für Public-Key-Objekte da,
// die die Funktion encrypt beherrschen.
class PublicKey{
	public:
	PublicKey(mpz_class, mpz_class);
	PublicKey(string);
	~PublicKey();
	mpz_class getE(){return e;};
	mpz_class getN(){return n;};
	mpz_class encrypt(mpz_class);
	/*void printInfo(){
	cout << endl << "PublicKey: Modul n: " << n << endl <<
	"Öffentlicher Exponent e: " << e << endl;
	};*/
	void dumpToFile(string filename){
		ofstream pub(filename.c_str());
		pub << "e: " << getE() << endl << "n: " << getN();
		pub.close();
	};

	// siehe nach KeyGenerator
	char* generateSessionkeyAndEncryptIt(string&, int&);

	private:
	mpz_class e; // öffentlicher Exponent
	mpz_class n; // Modulus
};
// Konstruktor, für die Erzeugung eines Objektes
PublicKey::PublicKey(mpz_class publicExponent, mpz_class modul){
		e=publicExponent;
		n=modul;
	}
PublicKey::PublicKey(string filename){
		string temp;
		ifstream lese(filename.c_str());
		lese >> temp; // erste Zeile unbrauchbar
		lese >> temp; // zweite Zeile enthält e
		e=mpz_class(temp); // e wird umgewandelt in Zahl
		lese >> temp;
		lese >> temp;
		n=mpz_class(temp);
		lese.close();
	}
PublicKey::~PublicKey(){
}
// Verschlüsselung
mpz_class PublicKey::encrypt(mpz_class m){
	if(!(m<n)){
		cout << "Klartext zu groß!";
		return NULL;
	}
	mpz_t c;
	mpz_init(c);
	// c = m^e mod n
	mpz_powm(c, m.get_mpz_t(), e.get_mpz_t(), n.get_mpz_t());
	return *new mpz_class(c);
}





// Diese Klasse besitzt ein Public-Key-Objekt und beherrscht die Funktion decrypt
class PrivateKey{
	public:
	PrivateKey(mpz_class, mpz_class, mpz_class, mpz_class, mpz_class, mpz_class);
	PrivateKey(string);
	~PrivateKey();
	PublicKey getPK(){return *p;}; // Gibt den Public-Key zurück
	mpz_class decrypt(mpz_class);
	/*void printInfo(){
	p->printInfo();
	cout << endl << "Privater Schlüssel: Geheimer Exponent d: " << d << endl
	 << "Faktor p: " << faktor_p << endl << "Faktor q: " << faktor_q << endl
	 << "Multiplikativ Inverses u von p mod q: " << modinv_u << endl;
	};*/
	/*mpz_class getD(){return d;};
	mpz_class getP(){return faktor_p;};
	mpz_class getQ(){return faktor_q;};
	mpz_class getU(){return modinv_u;};*/
	void dumpToFile(string filename){
		ofstream priv(filename.c_str());
		priv << "e:" << endl << getPK().getE() << endl << "n:" << endl << getPK().getN() << endl << "d:" << endl << d << endl << "p:" << endl << faktor_p << endl << "q:" << endl << faktor_q << endl << "u:" << endl << modinv_u;
		priv.close();
	};
	
	// Entschlüsselt einen Sitzungsschlüssel und liefert den Zeiger auf das Array.
	// Der letzte Parameter wird überschrieben und enthält danach die Länge des Arrays.
	// Liefert NULL, wenn der session-key ungültig ist oder ein falscher Private-Key verwendet wird
	char* decryptSessionkey(string c, int& schluesselLaenge){
		mpz_class k=decrypt(mpz_class(c, 16)); // umwandeln von hex. und entschlüsseln
		
		// Kontrollieren, ob alle Schlüssel da sind
		char kontrollbyte=0;
		int len=mpz_sizeinbase(k.get_mpz_t(), 2);
		for(int i=0; i<8; i++){
			if(mpz_tstbit(k.get_mpz_t(), len-1-i)==1){
				kontrollbyte^= ((char)1) << (7-i);}
		}
		if(kontrollbyte!='\xfe'){
			cout << "Ungültiger Sitzungsschlüssel oder falscher Private-Key" << endl;
			return NULL;
		}
		schluesselLaenge=len/8-1;
		// Schlüssel wieder in Binärform bringen
		char* schluessel=new char[schluesselLaenge];
		for(int i=0; i<schluesselLaenge; i++){
			schluessel[i]=0;
			for(int j=0; j<8; j++){
				if(mpz_tstbit(k.get_mpz_t(), i*8+j)==1){
					schluessel[i]^= ((char)1) << j;}
			}
		}

		/*cout << "Schlüssel: ";
		for(int i=0; i<schluesselLaenge; i++){
			cout << hex << setw(2) << int((unsigned char)schluessel[i]);
		}
		cout << endl;
		cout << endl << "k: " << k.get_str(16) << endl << endl;
		cout << "Schlüssellänge: " << dec << schluesselLaenge << endl;*/
		
		return schluessel;
	};

	private:
	PublicKey *p; // Zeiger auf das dazugehörige Public-Key-Objekt mit den Informationen e und n
	mpz_class d; // eigentlicher privater Schlüssel
	mpz_class faktor_p; // die restlichen Informationen werden für eine schnellere Entschlüsselung
	mpz_class faktor_q; // oder Signierung durch den Chinesischen Restsatz benötigt
	mpz_class modinv_u; // Multiplikativ Inverses von p mod q
	mpz_t d1; // Speichert das Ergebnis, welches in decrypt benötigt wird als Typ und nicht als Klasse (schneller)
	mpz_t d2; // Somit sind für das Entschlüsseln/Signieren diese Schritte nicht mehr durchzuführen
};
// Der Konstruktor verlangt die genauen Zahlen und wird von KeyGenerator aufgerufen
PrivateKey::PrivateKey(mpz_class e, mpz_class n, mpz_class d_privat, mpz_class fp, mpz_class fq, mpz_class u){
		p=new PublicKey(e, n);
		d=d_privat;
		faktor_p=fp;
		faktor_q=fq;
		modinv_u=u;
		mpz_class dt1=d%(faktor_p-1); // d kann bei der Verwendung als Exponent mod p in dieser Art verkleinert werden
		mpz_class dt2=d%(faktor_q-1); // weil x^(dp+φ(p) mod p = x^dp · x^φ(p) mod p = x^dp · 1 mod p
		mpz_init(d1); // mpz_t-Typen müssen initialisiert werden
		mpz_init(d2);
		mpz_set(d1, dt1.get_mpz_t()); // Somit werden die Zahlen direkt als Typ
		mpz_set(d2, dt2.get_mpz_t()); // und nicht als Klasse gespeichert → eine spätere Umrechnung fällt weg
	}
PrivateKey::PrivateKey(string filename){
		string temp;
		ifstream lese(filename.c_str()); // Einzele Zeilen auslesen und in Schlüssel umwandeln
		lese >> temp; // unbrauchbare Zeile
		lese >> temp;
		mpz_class e(temp);
		lese >> temp;
		lese >> temp;
		mpz_class n(temp);
		p=new PublicKey(e, n);
		lese >> temp;
		lese >> temp;
		d=mpz_class(temp);
		lese >> temp;
		lese >> temp;
		faktor_p=mpz_class(temp);
		lese >> temp;
		lese >> temp;
		faktor_q=mpz_class(temp);
		lese >> temp;
		lese >> temp;
		modinv_u=mpz_class(temp);
		lese.close();
		
		mpz_class dt1=d%(faktor_p-1); // d kann bei der Verwendung als Exponent mod p in dieser Art verkleinert werden
		mpz_class dt2=d%(faktor_q-1); // weil x^(dp+φ(p) mod p = x^dp · x^φ(p) mod p = x^dp · 1 mod p
		mpz_init(d1); // mpz_t-Typen müssen initialisiert werden
		mpz_init(d2);
		mpz_set(d1, dt1.get_mpz_t()); // Somit werden die Zahlen direkt als Typ
		mpz_set(d2, dt2.get_mpz_t()); // und nicht als Klasse gespeichert → eine spätere Umrechnung fällt weg
}
// Destruktor
PrivateKey::~PrivateKey(){
	delete p; // Public-Key-Objekt im Speicher löschen.
}
mpz_class PrivateKey::decrypt(mpz_class c){
	if(!(c<p->getN())){
		cout << "Geheimtext zu groß!"; // c muss kleiner als n sein
		return NULL;
	}
	mpz_t m1;
	mpz_init(m1);
	mpz_t m2;
	mpz_init(m2);
	// Lösung der Kongruenzen mit dem Chinesischen Restsatz
	// Es sind kleinere Berechnungen durchzuführen, was die
	// Geschwindigkeit erhöht
	mpz_powm(m1, c.get_mpz_t(), d1, faktor_p.get_mpz_t());
	// m1 = c^d1 mod p
	mpz_powm(m2, c.get_mpz_t(), d2, faktor_q.get_mpz_t());
	// m2  = c^d2 mod q
	mpz_class h=mpz_class(m2)-mpz_class(m1);
	if(h<0){
	h+=faktor_q; }
	h=(modinv_u * h) % faktor_q;
	mpz_class m=mpz_class(m1)+(h*faktor_p);
	return m;
}




// Für die Schlüsselpaarerzeugung zuständig
class KeyGenerator{
	public:
	PrivateKey generateKeyPair(unsigned int); // generiert ein Schlüsselpaar
	KeyGenerator();
	static char *getRandom(int); // liefert eine Folge von Zufallsbytes
	static bool initialised;
};
// der Pseudozufallsgenerator wird durch die aktuelle Zeit in ms initialisiert
KeyGenerator::KeyGenerator(){
	time_t seconds;
	time(&seconds);
	srand((unsigned int) seconds);
	initialised=true;
}
// Liefert ein Schlüsselpaar mit einem Modulus der Länge 'bit' Bit
PrivateKey KeyGenerator::generateKeyPair(unsigned int bit){
	while((bit%16!=0 && bit!=0) || bit<32){
		cout << "Die Bitlänge soll ein Vielfaches von 16 sein!" << endl;
		bit-=bit%16;
		if(bit<32)
			bit=32;
		cout << "Neue Schlüssellänge: " << dec << bit << endl;
	}
	mpz_class e("65537"); // Standardwert für den öffentlichen Exponenten
	// weil er nur zwei binäre Einsen enthält und somit schnell mit dem in der GMP-Lib
	// eingebauten Square-And-Multiply-Algorithmus exponentieren kann, aber trotzdem groß ist.
	mpz_class q; // der größere Primfaktor q und
	mpz_class p; // der kleinere Primfaktor p
	mpz_class n; // der Zahl n
	unsigned int nbitsize=0;
	mpz_t vergleich;
	mpz_init(vergleich); // Für einen großen Abstand von p und q benötigt
do{
	mpz_t rand1; // Zufallszahl
	mpz_init2(rand1, bit/2); // der halben Länge des Modulus
	char *zufall1=getRandom(bit/16);
	// Füllen mit Zufallsbits
	for(unsigned int i=0; i<bit/16; i++){
		for(int n=7; n>=0; n--){
		if((zufall1[i] & (1 << n)) != 0 ){
			mpz_setbit(rand1, i*8+n);
		}
		}
		zufall1[i]=i*7%256; // in Speicher überschreiben
	}
	mpz_setbit(rand1, bit/2-1); // erstes
	mpz_setbit(rand1, 0); // und letztes Bit auf 1 setzten → groß genug und ungerade
	delete(zufall1); // Speicher freigeben, delete überschreibt den Speicher nicht
	
	mpz_t rand2; // die zweite Zufallszahl
	mpz_init2(rand2, bit/2);
	char *zufall2=getRandom(bit/16);
	// wird mit Zufallsbits gefüllt
	for(unsigned int i=0; i<bit/16; i++){
		for(int n=7; n>=0; n--){
		if((zufall2[i] & (1 << n)) != 0 ){
			mpz_setbit(rand2, i*8+n);
		}
		}
		zufall2[i]=i*7%256; // in Speicher überschreiben
	}
	mpz_setbit(rand2, bit/2-1); // erstes und
	mpz_setbit(rand2, 0); // letztes Bit auf 1 setzen
	delete(zufall2); // Speicher freigeben

do{
	while (!mpz_probab_prime_p(rand1, 50)){ // Miller-Rabin und andere
		mpz_add_ui(rand1, rand1, 2); // Verfahren testen, ob es eine Primzahl ist
  	}                                // falls nicht, wird sie um 2 erhöht (bleibt ungerade)
	p=mpz_class (rand1); // Die erste Primzahl p wurde gefunden
	
	while (!mpz_probab_prime_p(rand2, 50)){ // 50 bedeutet die Anzahl der Tests
		mpz_add_ui(rand2, rand2, 2);
  	}
	q=mpz_class (rand2); // Primzahl q wurde gefunden	
	if(p>q){ // p soll kleiner sein als q → Tausch falls p>q
		mpz_class t=p;
		p=q;
		q=t;
	}
	n=p*q; // der Modulus n wird berechnet
	nbitsize=mpz_sizeinbase(n.get_mpz_t(),2); // die Länge wird abgefragt
	if(nbitsize<bit){ // und auf die richtige Länge überprüft
		cout << nbitsize << " ist die falsche Länge, erneuter Durchlauf" << endl;
		// im nächsten Durchlauf werden die Zufallszahlen erhöht
		// indem die eins der ersten Bits auf 1 gesetzt wird
		if(rand()%2==0) // für eine der beiden Primzahlen
		mpz_setbit(rand1, bit/2-2-rand()%5);
		else
		mpz_setbit(rand2, bit/2-2-rand()%5);
	}
}while(nbitsize<bit); // Es wird weiter nach geeigneten Primfaktoren für n gesucht

	cout << endl << "Größe von N: " << nbitsize << " Bit" << endl;
	// wenn p-q kleiner ist als 2·n^¼, ist es einfach p und q zu bestimmen
	mpz_class temporaer((q-p)/2);
	mpz_pow_ui(vergleich, temporaer.get_mpz_t(), 4);
	if(mpz_class(vergleich)<n){
		cout << "p und q zu nah aneinander" << endl;
	}
//wenn p und q zu nah aneinander liegen weiter suchen
}while(mpz_class(vergleich)<n);

	// p und q sind gefunden, φ(n) wird bestimmt
	mpz_class phi=(p-1)*(q-1);
	// falls e und phi nicht teilerfremd sind, wird e erhöht
	while(erwEuklid(e, phi)[2]!=1){
		e+=2;
		cout << endl << "e nicht teilerfremd zu phi!" << endl;
	}
	mpz_class d=erwEuklid(e, phi)[0] % phi; // Addieren und Subtrahieren ergibt gültiges d:
	if(d<0){
	d+=phi; } // Alle Zahlen sollen positiv sein.
	// Erhöhen um φ(n) bedeutet dennoch gültiges d, da nur das unwichtige k sich in der Gleichung ändern würde:
	// e·(d+φ(n))=k·φ(n)+1
	// <=> e·d+e·φ(n)=k·φ(n)+1 |-e·φ(n)
	// <=> e·d=k·φ(n)-e·φ(n)+1
	// <=> e·d=(k-e)·φ(n)+1
	mpz_class u=erwEuklid(p, q)[0] % q; // u wird als mult. Inverses zu p modulo q gesucht
	if(u<0){
	u+=q; } // Alle Zahlen sollen positiv sein
	// das Schlüsselpaar wurde erzeugt
	return *new PrivateKey(e, n, d, p, q, u);
}
// eine Zufallsfolge von Bytes der Länge 'len' wird erzeugt
char *KeyGenerator::getRandom(int len){
	if(initialised==false){
		time_t seconds;
		time(&seconds);
		srand((unsigned int) seconds);
		initialised=true;
	}
	char *zufallsfolge=new char[len];
	ifstream guterzufall("/dev/random"); // das Unix-Gerät für hardwareabhängigen guten Zufall
	ifstream nichtblockenderzufall("/dev/urandom"); // und das Gerät für schnellen Zufall wird angefordert
	if(guterzufall.is_open()){ // Geraete ist verfügbar
	 cout << endl << "Generiere Zufall: Bewegen Sie die Maus, tippen Sie, arbeiten Sie mit dem PC und benutzen Sie die Festplatte." << endl;
	 if(!nichtblockenderzufall.is_open()){
	 	cout << "/dev/urandom ist nicht verfügbar, Zeitabhängiger Pseudo-Zufall wird teilweise hinzugezogen." << endl;
		}
		for(int i=0; i<len; i++){
			if(i%8==0){ // Jedes 8. Byte ist echter, blockender Zufall
			 cout << "*" << flush;
			 guterzufall.get(zufallsfolge[i]);
			}else{ // der Rest ist Pseudozufall
				if(nichtblockenderzufall.is_open()){ // entweder gut
				nichtblockenderzufall.get(zufallsfolge[i]);
				}else{ // oder weniger gut, wie dieser zeitabhängige Zufall
				zufallsfolge[i]=char(256.0*rand()/(RAND_MAX + 1.0));
				}
			}
		}
		guterzufall.close();
		nichtblockenderzufall.close();
		cout << endl;
	}else{ // es sind keine Zufallsgeräte verfügbar, es wird ein zeitabhängiger Pseudozufall verwendet
		cout << endl << "/dev/random ist nicht verfügbar, Zeitabhängiger Pseudo-Zufall muss benutzt werden." << endl;
		for(int i=0; i<len; i++){
			zufallsfolge[i]=char(256.0*rand()/(RAND_MAX + 1.0));
		}
	}
	return zufallsfolge;
}

bool KeyGenerator::initialised=false;

// Erzeugt einen Sitzungsschlüssel und liefert den Zeiger auf das Array.
// Die beiden Parameter werden überschrieben und enthalten danach
// den verschlüsselten session-key als Hex-String und die Länge des Arrays
char* PublicKey::generateSessionkeyAndEncryptIt(string& c, int& schl_len){
		
		// Größe wird angepasst, um ihn mit einem Mal durch den Modulus zu verschlüsseln
		schl_len=mpz_sizeinbase(getN().get_mpz_t(),2); // Länge des Modulus in Bit begrenzt den Sitzungsschlüssel
		schl_len-=24; // 24 Bit weniger wegen Kontrollbyte FE (Hex) bzw. 254 (Dec), 1 Pufferbyte und kleiner als der Modulus
		if(schl_len>2048)
			schl_len=2048; // Maximum für die zu RC4 kompatible Stromchiffre
		schl_len/=8; // Die Schlüssellänge wird als Byteanzahl benötigt
		
		char* schluessel=KeyGenerator::getRandom(schl_len); // Wird mit Zufallszahlen gefüllt
		
		mpz_t k; // Der Schlüssel wird von einem Byte-Array zu einer Zahl konvertiert
		mpz_init(k);
		for(int i=0; i<schl_len; i++){
			for(int n=0; n<8; n++){
			if((schluessel[i] & (1 << n)) !=0){
				mpz_setbit(k, i*8+n); // Jedes Byte des Schlüssels wird in 8 Bits der Zahl zerlegt
			}
			}
		}
		// An den Anfang des Sitzungsschlüssels wird vor dem Verschlüsseln mit dem Public-Key
		// ein Kontrollbyte hinzugefügt, das es erlaubt, während der Entschlüsselung zu überprüfen,
		// ob der Sitzungsschlüssel gültig ist
		for(int n=0; n<8; n++){
			if(('\xfe' & (1 << n)) !=0)
				mpz_setbit(k, schl_len*8+n);
		}
		
		// hexadezimale Ausgabe des Schlüssels
		/*cout << "Schlüssel: ";
		for(int i=0; i<schl_len; i++){
			cout << hex << setw(2) << int((unsigned char)schluessel[i]);
		}*/
		c=encrypt(mpz_class(k)).get_str(16);
		
		// hexadezimale Ausgabe des Schlüssels
		//cout << endl << "k: " << mpz_class(k).get_str(16) << endl;
		//cout << "Schlüssellänge: " << dec << schl_len << endl;
		
		return schluessel;
	}




// Symmetrische Kryptographie

void strom_chiffre(char* content, unsigned long content_length, char* key, int key_length)
{   // zu ARC4 (Alleged-RC4) kompatibel
    unsigned char S[256]; //S-Box
    unsigned int i=0,j=0; unsigned long n=0; //Laufvariablen
    unsigned char temp;
    //KSA (key-scheduling algorithm) - Berechnet die S-Box
    for(i=0;i<256;i++){
		S[i] = i;
		}
    for (i=j=0;i<256;i++){
        j = (j + key[i % key_length] + S[i]) & 255;
        temp = S[i];
        S[i] = S[j];
        S[j] = temp;
    }
    i=j=0;
	// Eine Attacke ermöglicht, aus den ersten 256 Byte auf die S-Box zu schließen
	for(int weg=0; weg<256; weg++){
		// Führe Pseudo-Zufallsgeneration aus
        i = (i + 1) & 255;
        j = (j + S[i]) & 255;
        temp = S[i];
        S[i] = S[j];
        S[j] = temp;
	}
    for (n=0; n<content_length; n++) // Läuft den gesamten Inhalt durch
    {
        // PRGA (pseudo-random generation algorithm)
        i = (i + 1) & 255;
        j = (j + S[i]) & 255;
        temp = S[i];
        S[i] = S[j];
        S[j] = temp;

        content[n] = content[n] ^ S[(S[i] + S[j]) & 255];  //XOR Verknüpfung mit dem Inhalt
    }
}

// Dateigröße
unsigned long filesize(const char* sFileName)
{
  ifstream f;
  f.open(sFileName, ios_base::binary | ios_base::in);
  if (!f.good() || f.eof() || !f.is_open()) { return 0; }
  f.seekg(0, ios_base::beg);
  ifstream::pos_type begin_pos = f.tellg();
  f.seekg(0, ios_base::end);
  unsigned long temp=static_cast<unsigned long>(f.tellg() - begin_pos);
  f.close();
  return temp;
}


// Datei-Verschlüsseln ohne in den RAM zu laden
void arc4DateiVerschluesselung(string filename, char* key, int key_length){
	// zu ARC4 (Alleged-RC4) kompatibel
    unsigned char S[256]; //S-Box
    unsigned int i=0,j=0; unsigned long n=0; //Laufvariablen
    unsigned char temp;
    //KSA (key-scheduling algorithm) - Berechnet die S-Box
    for(i=0;i<256;i++){
		S[i] = i;
		}
    for (i=j=0;i<256;i++){
        j = (j + key[i % key_length] + S[i]) & 255;
        temp = S[i];
        S[i] = S[j];
        S[j] = temp;
    }
    i=j=0;
	// Eine Attacke ermöglicht, aus den ersten 256 Byte auf die S-Box zu schließen
	for(int weg=0; weg<256; weg++){
		// Führe Pseudo-Zufallsgeneration aus
        i = (i + 1) & 255;
        j = (j + S[i]) & 255;
        temp = S[i];
        S[i] = S[j];
        S[j] = temp;
	}
	
	// Dateiverschlüsselung
	unsigned long len=filesize(filename.c_str());
	unsigned long blocksize=1024*1024*10; // 10 MiB
	char* data=new char[blocksize];
	unsigned long gelesen=0;
	
	fstream leseschreibe(filename.c_str(), ios::binary | ios::in | ios::out);
	leseschreibe.seekg(0, ios_base::beg);
	fstream::pos_type position;
	
	while(gelesen<len){
		if(blocksize>len-gelesen){
			blocksize=len-gelesen;
			delete[] data;
			data=new char[blocksize];
		}
		position=leseschreibe.tellp();
		leseschreibe.read(data, blocksize);
		leseschreibe.seekg(position, ios::beg);
		
		for (n=0; n<blocksize; n++) // Läuft den gesamten Inhalt durch
   		{
        // PRGA (pseudo-random generation algorithm)
        i = (i + 1) & 255;
        j = (j + S[i]) & 255;
        temp = S[i];
        S[i] = S[j];
        S[j] = temp;

        data[n] = data[n] ^ S[(S[i] + S[j]) & 255];  //XOR Verknüpfung mit dem Inhalt
    	}
		leseschreibe.write(data, blocksize);
		gelesen+=blocksize;
	}
	
	leseschreibe.close();
    
}

// ~~~~~~~~~~ 2 Dateien
// ####################################################################

// Datei-Verschlüsseln ohne in den RAM zu laden
void arc4DateiVerschluesselung(string inputfilename, string outputfilename, char* key, int key_length){
	// zu ARC4 (Alleged-RC4) kompatibel
    unsigned char S[256]; //S-Box
    unsigned int i=0,j=0; unsigned long n=0; //Laufvariablen
    unsigned char temp;
    //KSA (key-scheduling algorithm) - Berechnet die S-Box
    for(i=0;i<256;i++){
		S[i] = i;
		}
    for (i=j=0;i<256;i++){
        j = (j + key[i % key_length] + S[i]) & 255;
        temp = S[i];
        S[i] = S[j];
        S[j] = temp;
    }
    i=j=0;
	// Eine Attacke ermöglicht, aus den ersten 256 Byte auf die S-Box zu schließen
	for(int weg=0; weg<256; weg++){
		// Führe Pseudo-Zufallsgeneration aus
        i = (i + 1) & 255;
        j = (j + S[i]) & 255;
        temp = S[i];
        S[i] = S[j];
        S[j] = temp;
	}
	
	// Dateiverschlüsselung
	unsigned long len=filesize(inputfilename.c_str());
	unsigned long blocksize=1024*1024*10; // 10 MiB
	char* data=new char[blocksize];
	unsigned long gelesen=0;
	
	ifstream lese(inputfilename.c_str(), ios::binary | ios::in);
	ofstream schreibe(outputfilename.c_str(), ios::binary | ios::out);

	while(gelesen<len){
		if(blocksize>len-gelesen){
			blocksize=len-gelesen;
			delete[] data;
			data=new char[blocksize];
		}
		lese.read(data, blocksize);
		
		for (n=0; n<blocksize; n++) // Läuft den gesamten Inhalt durch
   		{
        // PRGA (pseudo-random generation algorithm)
        i = (i + 1) & 255;
        j = (j + S[i]) & 255;
        temp = S[i];
        S[i] = S[j];
        S[j] = temp;

        data[n] = data[n] ^ S[(S[i] + S[j]) & 255];  //XOR Verknüpfung mit dem Inhalt
    	}
		schreibe.write(data, blocksize);
		gelesen+=blocksize;
	}
	
	lese.close();
	schreibe.close();
    
}



class ArgumentVerarbeiter{
	private:
	int argAnz;
	char** argArray;

	public:
	ArgumentVerarbeiter(int argc, char** argv){
		argAnz=argc;
		argArray=argv;
		};
	int woIstArgument(int argc, char** argv, string arg){
		// 0 heißt nicht drinne
		for(int i=1; i<argc; i++){
			if(string(argv[i])==arg)
				return i; // Gibt den Index des gefragten Strings zurück
		}
		return 0;
		};
	int getArgumentInt(string bezeichner, string frage="Geben Sie <bezeichner> ein (<minWert>-<maxWert>): ", int minWert=0, int maxWert=0){
		if((maxWert<minWert && maxWert!=0) || (minWert==maxWert && minWert==0)){
			minWert=numeric_limits<int>::min();
			maxWert=numeric_limits<int>::max();
		}
		if(maxWert<minWert && maxWert==0){
			maxWert=numeric_limits<int>::max();
		}
		if(frage=="Geben Sie <bezeichner> ein (<minWert>-<maxWert>): " || frage==""){
			stringstream raus;
			raus << minWert;
			string min = raus.str();
			stringstream raus2;
			raus2 << maxWert;
			string max = raus2.str();
			frage="Geben Sie "+bezeichner+" ein ("+min+"-"+max+"): ";
		}
		int temp=0;
		if(woIstArgument(argAnz, argArray, bezeichner)==0){
			while(temp<minWert || temp>maxWert){
				cout << frage;
				cin >> temp;
		}
		}else{
			// Argument nach <bezeichner> umwandeln in int
			temp=atoi(argArray[woIstArgument(argAnz, argArray, bezeichner)+1]);
			while(temp<minWert || temp>maxWert){
				cout << frage;
				cin >> temp;
			}
		}
		return temp;
		};
	string getArgumentStr(string bezeichner, string frage="Geben Sie <bezeichner> ein: "){
		if(frage=="Geben Sie <bezeichner> ein: " || frage==""){
			frage="Geben Sie "+bezeichner+" ein: ";
		}
		string temp="";
		if(woIstArgument(argAnz, argArray, bezeichner)==0){
			while(temp.length()<1){
				cout << frage;
				cin >> temp;
		}
		}else{
			temp=string(argArray[woIstArgument(argAnz, argArray, bezeichner)+1]);
		}
		return temp;
		};
	bool contains(string arg){
		if(arg==string(argArray[0]))
			return true;
		return woIstArgument(argAnz, argArray, arg)!=0;
	};
};


int main(int argc, char** argv)
{
	KeyGenerator k; // Liefert Zufallszahlen und Schlüsselpaare
	ArgumentVerarbeiter av(argc, argv);
	if((argc==1 || argc==2) && !av.contains("--new")){
		cout << "RSA-Beispielimplementierung durch Benutzung der" << endl << "GNU Multiple Precision Arithmetic Library <gmplib.org>" << endl <<
		"Vers. " << dec << VERSION << " Copyright © 2009 Kai Lüke <kai-tobias@web.de> oder auf kailueke.de.vu" << endl << endl <<
		"This program is free software; you can redistribute it and/or modify" << endl <<
		"it under the terms of the GNU General Public License as published by" << endl <<
		"the Free Software Foundation; either version 3 of the License, or" << endl <<
		"(at your option) any later version." << endl << endl <<
		"This program is distributed in the hope that it will be useful," << endl <<
		"but WITHOUT ANY WARRANTY; without even the implied warranty of" << endl <<
		"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the" << endl <<
		"GNU General Public License for more details." << endl << endl <<
		"You should have received a copy of the GNU General Public License" << endl <<
		"along with this program.  If not, see <http://www.gnu.org/licenses/>." << endl << endl <<
		"Das Programm ist enthält nur die RSA-Grundlagen und ist in dieser Form noch" << endl <<
		"nicht zur Praktischen Anwendung geeignet. Es fehlen:" << endl <<
		"· OAEP		· ein Protokoll" << endl <<
		"· Hybride Verschlüsselung mit AES, Twofish, Serpent o.ä." << endl <<
		"Das alles liefert der OpenPGP-Standard, welcher von GnuPG unterstützt wird," << endl <<
		"daher empfehle ich die Benutzung von GnuPG." << endl << "<Enter>";
		getchar();
		cout << endl << "~Hilfe~" << endl << " --help ist diese Seite" << endl <<
		" --new erzeugt ein neues Schlüsselpaar" << endl <<
		" --size <Zahl> gibt die zu verwendende Bitlänge des Modulus an" << endl <<
		" --public <Dateiname> gibt die Datei für den Public-Key an" << endl <<
		" --private <Dateiname> gibt die Datei für den Private-Key an" << endl <<
		" --encrypt verschlüsselt (ARC4) und erstellt <Dateiname>.schluessel" << endl <<
		" --decrypt entschlüsselt unter Verwendung von <Dateiname>.schluessel" << endl <<
		"           und dem Private-Key" << endl <<
		" --output <Dateiname> dort wird gespeichert (am besten so wie input), sonst cout" << endl <<
		" --input <Dateiname> diese Datei wird gelesen und ent-/verschlüsselt" << endl <<
		" ohne die letzte Angabe wird dezimal interaktiv ver- oder entschlüsselt" << endl <<
		" Beispiele: " << argv[0] << " --new --size 4096 --public pub.txt --private priv.txt" << endl <<
		"        erstellt ein Schlüsselpaar, pub.txt darf jeder haben" << endl <<
		"  " << argv[0] << " --encrypt --input viel.dat --output viel.dat --public pub.txt" << endl <<
		"        erzeugt viel.dat.schluessel (immer bei Datei) und verschlüsselt viel.dat"
		"  " << argv[0] << " --decrypt --private priv.txt --input viel.dat --output vielEnt.dat" << endl <<
		"        entschlüsselt viel.dat[.schluessel], speichert aber in vielEnt.dat" << endl;
		return 0;
	}
	
	// ####################################################################
	
	// --new ist in der Befehlszeile enthalten, also neues Schlüsselpaar erstellen
	if(av.contains("--new")){
		int schlLen=av.getArgumentInt("--size", "Geben sie die Schlüssellänge in Bit ein (ca. 1024 bis 4096): ", 32);
		cout << "Schlüssellänge: " << schlLen << endl;
		
		// Schlüssel generieren lassen
		PrivateKey geheimschluessel=k.generateKeyPair(schlLen);
		PublicKey oeffentlich=geheimschluessel.getPK();

		// Dateiname des öffentlichen Schlüssels erfragen
		string pubname=av.getArgumentStr("--public", "Geben sie den Namen für die öffentliche Schlüsseldatei an: ");
		cout << "Public-Key-Dateiname: " << pubname << endl;
		// Öffentlichen Schlüssel in Datei schreiben
		oeffentlich.dumpToFile(pubname);
		
		string privname=av.getArgumentStr("--private", "Geben sie den Namen für die private Schlüsseldatei an: ");
		cout << "Private-Key-Dateiname: " << privname << endl;
		// Privaten Schlüssel in Datei schreiben
		geheimschluessel.dumpToFile(privname);
		cout << "Schlüsselpaar erfolgreich generiert!" << endl;
		return 0; // der --new-Modus ist vorbei, das Programm wird beendet
	}
	
	// ####################################################################
	
	// --encrypt-Modus
	if(av.contains("--encrypt")){
		string pubname=av.getArgumentStr("--public", "Geben sie den Namen für die öffentliche Schlüsseldatei an: ");
		if(filesize(pubname.c_str())==0){
			cout << "Öffentlicher Schlüssel nicht verfügbar" << endl;
			return 1;
		}
		PublicKey oeffentlich(pubname);
		
		// interaktiver Modus mit Eingabe und darauffolgender Verschlüsselung von Zahlen
		if(!av.contains("--input") || !av.contains("--output")){
			cout << "~Interaktiver Modus~" << "Beenden mit 'quit', 'exit' oder 'end'" << endl << "Gib Zahlen ein, die durch RSA verschlüsselt werden sollen…" << endl;
			string temp;
			cin >> temp;
			if(temp.length()==0 || temp=="end" || temp=="quit" || temp=="exit")
				return 0;
			do{
				cout << " " << oeffentlich.encrypt(mpz_class(temp)) << endl;
				cin >> temp;
			}while(temp.length()!=0 && temp!="end" && temp!="quit" && temp!="exit");
			return 0; // Ende des interaktiven Modus
		}
		// Datei-Modus
		
		// Wurde schon verschlüsselt
		if(filesize(string(av.getArgumentStr("--input")+".schluessel").c_str())!=0){
			cout << "Ist bereits verschlüsselt, da " << av.getArgumentStr("--input") << ".schluessel existiert!" << endl;
			return 1;
		}
		
		// Temporärer Schlüssel (Sitzungsschlüssel), mit ihm wird die Datei verschlüsselt
		int schl_len;
		string c;
		char* schluessel=oeffentlich.generateSessionkeyAndEncryptIt(c, schl_len);
		// Sitzungsschlüssel verschlüsselt in <Dateiname>.schluessel-Datei schreiben
		ofstream schreibe((av.getArgumentStr("--output")+".schluessel").c_str());
		schreibe << c;
		schreibe.close();
		
		if(av.contains("--output") && av.getArgumentStr("--output")==av.getArgumentStr("--input")){
			arc4DateiVerschluesselung(av.getArgumentStr("--output"), schluessel, schl_len);
		}else{
			arc4DateiVerschluesselung(av.getArgumentStr("--input"), av.getArgumentStr("--output"), schluessel, schl_len);
		}
		// Dateigröße ermitteln
		/*unsigned long fs=filesize(av.getArgumentStr("--input").c_str());
		ifstream lese2(av.getArgumentStr("--input").c_str(), ios::in | ios::binary);
		char* data=new char[fs];
		lese2.read(data, fs); // Datei in den RAM laden
		lese2.close();
		
		// ARC4 verschlüsseln
		strom_chiffre(data, fs, schluessel, schl_len);*/
		
		for(int i=0; i<schl_len; i++){schluessel[i]=255;}; // schluessel im Speicher überschreiben
		delete[] schluessel; // schluessel im Speicher freigeben
		// cout << "Größe: " << fs << endl;
		/*
		// neuen Dateiinhalt schreiben
		ofstream schreibe2(av.getArgumentStr("--output").c_str(), ios::out | ios::binary);
		schreibe2.write(data, fs);
		schreibe2.close();*/
		
		cout << "Fertig!" << endl;
		return 0; // --encrypt-Modus beenden
	}
	
	// ####################################################################
	
	// --decrypt-Modus
	if(av.contains("--decrypt")){
		string privname=av.getArgumentStr("--private", "Geben sie den Namen für die private Schlüsseldatei an: ");

		if(filesize(privname.c_str())==0){
			cout << "Private Schlüssel nicht verfügbar" << endl;
			return 1;
		}
		
		PrivateKey geheimschluessel(privname);
		
		// Interaktiver Modus, keine Dateiverschlüsselung
		if(!av.contains("--input")){
			string temp;
			cout << "~Interaktiver Modus~" << "Beenden mit 'quit', 'exit' oder 'end'" << endl << "Gib Zahlen ein, die durch RSA verschlüsselt werden sollen…" << endl;
			cin >> temp;
			if(temp.length()==0 || temp=="end" || temp=="quit" || temp=="exit")
				return 0;
			do{
				cout << " " << geheimschluessel.decrypt(mpz_class(temp)) << endl;
				cin >> temp;
			}while(temp.length()!=0 && temp!="end" && temp!="quit" && temp!="exit" && temp!="help");
			return 0;
		}
		// Dateientschlüsselung
		// überprüfen, ob die Sitzungsschlüsseldatei da ist
		if(filesize(string(av.getArgumentStr("--input")+".schluessel").c_str())==0){
			cout << av.getArgumentStr("--input") << ".schluessel ist nicht vorhanden!" << endl <<
			"Diese Datei enthält den verschlüsselten ARC4-Schlüssel der Datei!" << endl;
			return 1; // nicht da, Abbruch
		}
		ifstream lese(string(av.getArgumentStr("--input")+".schluessel").c_str());
		string tkey;
		lese >> tkey; // verschl. Sitzungsschlüssel auslesen
		lese.close();
		
		int schluesselLaenge=0;
		char* schluessel=geheimschluessel.decryptSessionkey(tkey, schluesselLaenge);
		if(schluessel==NULL)
			return 1; // Unpassender session key
		//cout << "Größe: " << dec << fs << endl; // Dateigröße
		
		// entweder in Datei schreiben oder in die Standardausgabe
		if(av.contains("--output")){
			if(av.getArgumentStr("--output")==av.getArgumentStr("--input")){
			arc4DateiVerschluesselung(av.getArgumentStr("--output"), schluessel, schluesselLaenge);
			remove(string(av.getArgumentStr("--input")+".schluessel").c_str());
			}else{
			arc4DateiVerschluesselung(av.getArgumentStr("--input"), av.getArgumentStr("--output"), schluessel, schluesselLaenge);
			}
			for(int i=0; i<schluesselLaenge; i++){schluessel[i]=255;}; // schluessel im Speicher überschreiben
			delete[] schluessel;
			cout << "Fertig!" << endl;
		}else{
			// Datei einlesen
			unsigned long fs=filesize(av.getArgumentStr("--input").c_str());
			lese.open(av.getArgumentStr("--input").c_str(), ios::in | ios::binary);
			char* data=new char[fs];
			lese.read(data, fs);
			lese.close();
			strom_chiffre(data, fs, schluessel, schluesselLaenge);
			for(int i=0; i<schluesselLaenge; i++){schluessel[i]=255;}; // schluessel im Speicher überschreiben
			delete[] schluessel;
			cout << data << endl;
		}
		return 0; // --decryption-Modus vorbei
	}
	// Das wars…
}
